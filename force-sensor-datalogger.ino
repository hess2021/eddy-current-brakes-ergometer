

#include <SPI.h>
#include <SD.h>

const int chipSelect = 10;
const String file = "u.csv";

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    while (1);
  }
  Serial.println("card initialized.");
  
  File dataFile = SD.open(file, FILE_WRITE);

  // if the file is available, write the header
  if (dataFile) {
    dataFile.println("Timestamp,Sensor1,Sensor2");
    dataFile.close();
    // print to the serial port too:
    Serial.println("Timestamp,Sensor1,Sensor2");
  }
}

void loop() {
  String dataString = "";

  //read sensors and format for csv
  int sensor1 = analogRead(1);
  int sensor2 = analogRead(2);
      dataString = millis();
      dataString += ",";
      dataString += sensor1 * (5.0 / 1023.0);
      dataString += ",";
      dataString += sensor2 * (5.0 / 1023.0);
      dataString += ",";
      dataString += "";
      dataString += ",";
    
  

  // open the file. note:only one file can be open at a time, close this before opening another.
  File dataFile = SD.open(file, FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening file");
  }
}
