\documentclass[12pt]{article}
% LAYOUT %
\usepackage[margin=1in]{geometry}
\usepackage[rightcaption]{sidecap}
\usepackage{caption}
\usepackage{subcaption}
% MATH %
\usepackage{bm}
\usepackage{amsmath}
\usepackage{textcomp,gensymb}
% STYLE %
\usepackage{fontspec}
\setmonofont{mononoki}[Scale=0.9]
\usepackage{rotating} % rotating text \begin{turn}{45}
\usepackage{setspace} % \{single,one-half}space \doublespacing
\usepackage{enumitem} % \begin{itemize}[itemsep=0.1mm,parsep=0pt]
\usepackage[hidelinks]{hyperref} % \href{url}{text}
\hypersetup{colorlinks=true,linkcolor=black,urlcolor=cyan,filecolor=blue}
% MEDIA %
\usepackage{tikz}
%\usepackage{circuitikz}
\usepackage{graphicx}
% CITATIONS %
%\usepackage[backend=biber,style=mla]{biblatex}
%\addbibresource{file.bib}

\title{\vspace{-1cm}{\small\textbf{Bowdoin Physics\\}}\vspace{8cm}{\textbf{\Large Designing, Characterizing, and Testing Eddy Current Brakes for a Feedback-Driven Upper Body Ergometer}}\vspace{7cm}}
\author{\normalsize \textbf{Luke Bartol} and \textbf{Lorenzo Hess}\\{\normalsize PHYS 3010}}
\date{\normalsize May 18, 2022}

\begin{document}
\maketitle

\newpage
\tableofcontents

\renewcommand\ni{\noindent}
\newcommand\ic[1]{\texttt{#1}} % [i]nline [c]ode

%%%% TODO %%%%
% Labview VI screenshot
% Luke's roadmap bullets
% Calculation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
{\small\emph{In this report, paragraphs beginning with \underline{underlines} were written by Luke, and the others by Lorenzo.}}
\begin{center}$\hdots$\end{center}

\underline{From treadmills} to stationary bikes to rowers, there have been a plethora of machines designed to replicate outdoor athletic activities in an indoor, stationary environment. While these are often great options for rainy days and are better than just jogging in place, they are often a poor replacement for the real thing, serving just as a training tool to replicate in the interim. Skiing is such a sport, which often requires a replacement in the summer months when there is no snow around, thus a whole class of ski imitation gear has been created to fill this niche. One such technology is a ski ergometer (often referred to as a ski-erg or skerg), which allows a skier to practice a poling motion and engage specific muscles needed for skiing.

\underline{Since it} lacks the full-body imitation of something like roller skiing, the use of the ski-erg is often focused on building muscle and endurance in this specific motion. Despite this, many skiers will tell you that while it potentially engages the same muscles, the feel is quite different. This can be attested to by the fact that all the records on this machine are taken by bulky crossfitters doing a weird sort of squatting motion, rather than pro nordic skiers.

\underline{The issue} with these machines and many ergs in general is that they provide resistance to the user through a flywheel or fan using friction. While this overall friction can be adjusted between sessions, the resistance provided is just subject to the laws of physics, which is often not the ideal that one would look for. This project endeavored to create an ergometer that could have variable resistance both within one stroke, but also between consecutive strokes, so that it could better resemble reality. By measuring force data from real skiing, using this as a model for resistance, and then comparing the new erg's force data to that of existing ergs, it would prove the improvement of this technology. While this project was created with skiing in mind, there is no doubt that other sports/machines would benefit similarly from this technology.

At its core, the goal of this project is to build a feedback mechanism to control the braking force on an erg. We achieved this, using Labview to handle the feedback logic and control the necessary hardware. Our ergometer consists of a mounted bike wheel and chain. We use eddy current physics to create our braking force.

The bike wheel has an aluminum fin around its circumference which spins between the poles of a toroidal electromagnet. Following Lenz's Law, the eddy currents induced in the aluminum fin induce a magnetic field opposite in direction to the driving B-field. The opposing B-fields repel each other and halt the wheel. Eddy currents are ideal to use in feedback-driven brakes because no friction is needed to brake, so the brakes do not wear out. On the contrary, providing feedback to physical brakes would involve taking into account their level of wear, which may be difficult to implement and not extensible beyond our particular experiment.

Early testing shows that we can indeed provide resistance feedback that qualitatively matches the resistance of actual exercise. We later identify the many limitations of our machine and the steps that future students may take to develop the project further.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Literature Review}

\underline{The literature} review for this project fell into two main categories: 1) that surrounding force measurements of skiers, both regarding previously gathered data and methods of gathering data; and 2) the theory and physical implementation of eddy current brakes.

\subsection{Forces Involved in Existing Ergs}
Some papers like Ohtonen (2019) and Stöggl and Holmberg (2011) discussed models of these forces with idealized representations of things like force curves, shown in figures \ref{fig:curve-1} and \ref{fig:curve-2} below. Others, such as Johansson et al (2019) and Smith (2017) developed ways to gather data from actual skiers or rollerskiiers, using sensors on the poles. These provided useful references to compare our own data to to confirm its validity and guidance in creating a model dataset. While we couldn’t directly replicate the pole sensors used by these studies, they provided a helpful starting point.

\begin{SCfigure}[0.7][h]
	\includegraphics[scale=0.4]{./images/ohtonen-curve.png}
	\caption{\small Recorded force curve via Ohtonen (2019)}
	\label{fig:curve-1}
\end{SCfigure}
\begin{SCfigure}[0.7][h]
	\includegraphics[scale=0.2]{./images/second-lit-curve.png}
	\caption{\small Recorded force curve via Stöggl and Holmberg (2019)}
	\label{fig:curve-2}
\end{SCfigure}

\underline{While there} were no previous examples of ski, row, or swim ergometers with this sort of variable resistance, a number of papers did something similar with a bicycle ergometer. Going as far back as Karpovich (1950) there were examples of electromagnetically braked bike ergs, with further examples from Seifert (1990), Kickinyov (2007), McCall and Smellie, and Bonde-Petersen (1974).

\subsection{Eddy Current Braking}

Our primary goal in exploring the literature on eddy current braking was to understand the feasibility of our project. Four papers were of use. Karakoc, Suleman, and Park provide an introductory survey of the history of eddy current braking and confirm that ``frequency modulated time varying fields improve the brakign torque significantly'' (1169). Gosline, Campion, and Hayward test eddy current brakes as haptic feedback mechanisms. They provide a simpler and more useful analysis that Karakoc et al., which can likely be used to validate future datasets (Fig. \ref{fig:gosline}).

\begin{SCfigure}[0.7][ht]
	\includegraphics[scale=0.3]{./images/gosline.png}
	\caption{\small An solid disc rotating between the poles of a toroidal electromagnet (Gosline et al.)}
	\label{fig:gosline}
\end{SCfigure}

The analytical equation Gosline et al. present for the torque on the wheel due to the magnet $\tau_d$ in the above diagram is:

\[ \tau_d = \frac{\pi}{4\rho}D^2dB^2R^2\dot{\theta}  \]

where $d$ is the thickness of the disc and $\rho$ is the specific resistivity of the conductor. Given this formula, we then prioritized finding an electromagnet with large poles and a strong field, as well as ensuring the aluminum plates in which eddy currents would be induced were at the greatest radius possible for the bike wheel, since the torque is quadratic in those variables.

Wiederick, Gauthier, and Campbell confirm these findings with a more general treatment of eddy current braking with toroidal electromagnets. They find the drag force

\[ F = \alpha\sigma (lw)\delta B_0^2v \]

Gosline et al.'s torque equation is recovered with $F\cdot R$, and with the conversions: $\sigma = 1/\rho$, $lw \propto D^2$ as the area in flux, $\delta = d$, and $v = R\dot{\theta}$ (with $\alpha$ as a dimensionless paramter).

Finally, Ma and Shiau provide an analysis of four separate experimental setups. Most importantly, they are able to decelerate a 250kg mass, dropped from a height of 3m, to a 1m/s terminal velocity using permanent magnets in the range of 0.1-1T. In other words, even small magnetic fields (e.g. 0.5T in the paper) can be used to halt large masses, which provides good confirmation that a bike wheel can be halted. The circumference of a bike wheel is 4.3m, and given that the wheel will make many revolutions through one stroke, the effective ``drop height'' of the aluminum attached to the wheel would be several times greater than that of the 250kg mass, suggesting that even smaller fields (e.g. 0.2T) might be sufficient to quickly halt the wheel.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Design}

\subsection{Force Curves}

\subsubsection{Acquiring force curves from existing machines}
\underline{The first} part of this experiment involved getting a way to measure and compare force curves, both to create a sample curve and to compare our final product to existing machines. It was determined that the easiest way of acquiring suitable sensors was to repurpose two Etekcity 110lb Luggage Scales (\texttt{UPC 00679113374744}) for their internal force sensors. This was referenced in several blog posts online, though none had clear instructions to follow. Upon removal from their housing, the sensors were found to produce just a few millivolts of signal, not enough to reliably register using LabView or an Arduino as hoped. After the construction of a mechanism to allow the sensors to be battery powered and connected to the different apparatuses, the focus turned to amplification of their signal. While an attempt was made to use op-amps, the small signal, large gain, and portable nature made for many difficulties, so it was ultimately determined that using an existing amplifier box would be the best path forward. The sensors were calibrated using known weights (Fig. \ref{fig:calibr-curve}), which was most helpful in showing the linear relationship of the sensors. These sensors were used to record force curves, discussed more below.

\begin{SCfigure}[0.4][h]
	\centering
	\includegraphics[scale=0.25]{./images/calibr-curve.png}
	\caption{Force sensor calibration curve}
	\label{fig:calibr-curve}
\end{SCfigure}


\subsection{Feedback}

The ability to provide feedback to our electromagnet is central to our design goal.

In a finished product, it would be useful to have feedback:
\begin{enumerate}[itemsep=0.1mm,parsep=0pt]
	\item over the course of the stroke
	\item over long time scales to mimic changes in terrain
	\item in proportion to the user's strength
	\item in proportion to the angular direction of the stroke
\end{enumerate}

For a proof of concept, only feedback over the course of the stroke is necessary, as we only need to show that the braking force can be varied to successfully replicate the forces ocurring during real exercise. By definition, this entails varying the force as a function of position in the stroke. Feedback over long time scales is feedback with respect to time and is independent of the machine's mechanics, and is thus not critical for proof of concept.

Feedback in proportion to the user's strength, while necessary in a final product, is not strictly necessary in a proof of concept. A final product is used by people of greatly differing strengths. If they each used a ``one-size-fits-all'' ergometer, with no feedback proportional to strength, the weakest users would feel too much braking force and the strongest users would feel too little. While building a proof of concept, however, the weight used during testing can remain constant, or at least to within a small range, and the braking force does not need feedback from the driving force.

Feedback in proportion to the angular direction of the stroke would provide maximum resistance when the user pulls straight down and zero resistance when the user pulls parallel to the floor. Data would have to be taken of force as a function of ski pole angle to fill in the rest of the function. This is not crucial for a proof of concept.

In a final product, the braking force as a function of position in the stroke, $F(p)$, would ideally be continuous. However, it can be approximated with a stepwise function without users noting the difference. Continuous measurements of the wheel's position may even be too far into diminishing returns to make it worth it in a final product.

\subsubsection{Object Sensor Feedback}

We used an object sensor to keep track of the user's position in the stroke. The sensor, the \texttt{QRD1114}, consists of a semiconductor chip housing an infrared photodiode and an infrared phototransistor. The sensor goes high (5V) when it shines on a reflecting surface and goes low (0V) when it shines on a non-reflecting surface. A square wave is thus produced as the surface changes, with each rising or falling edge indicating a change in surface.

We placed the photodiode near the reflective rim of our revolving wheel and added four pieces of non-reflecting black tape at 90$\degree$ intervals on the rim. Thus, each falling edge in the diode's output corresponded with a quarter-revolution of the wheel. Our $F(p)$ now became $F(h)$, or force as a function of ``holes'' that have passed the diode. The pieces of black tape each covered a pre-drilled hole in the rim, hence the name of the independent variable.

Indeed, we had tried using the holes by themselves, but the reflecting rim was similar enough to whatever the object sensor received through the hole that the square wave peaked at about 4V. Moreover, the bevels on the edges of the holes, though less than 1mm in length, were sufficient to change the desired, quasi-discontinuous rising edge into a curve with an occasional dip, unsuitable for consistent detection by a computer.

Even with the black tape, the sensor's output still did not reach 5V, so we added highly reflective white index cards to the entire rim and then reattached the four pieces of black tape. The reflective contrast between the two produced a 0-5V square wave as desired. The straight edge between the tape and index card produced an ideal square wave.

\subsubsection{Labview}

We used Labview to read the object sensor's voltage output, process it, and then change the electromagnet's voltage input. There are two parts to our VI: 1) interfacing with the hardware (analog input from the object sensor and digital output from the computer); and 2) the feedback logic.

We used the \texttt{NI9215} and \texttt{NI9269} to interface with the object sensor and DC power supply. We had tried using the frequency counter in the \texttt{NI USB-6009} to track the rising edges of the object sensor, but were unable to get reliable counts.

The feedback logic was coded in a formula node, as is shown below. For every new rising edge in the photodiode's output, the electromagnet's voltage \texttt{V\_EM} is changed to the next element in a list. This list, programmed as \texttt{V\_of\_h}, for ``voltage as a function of holes,'' is the data structure that represents our stepwise ``force curve.'' The data is read from a \texttt{csv} file which is generated by the Arduino and then manually edited.

An example \texttt{V\_of\_h} list that mimics the ramp-up, slight dip, and ramp-down of an average force curve could be:\\

\texttt{V\_of\_h = [1, 1.5, 2, 3, 4, 6, 8, 8.5, 9, 8, 9, 7, 4, 2, 2, 1]}\\

On the diode's first rising edge, the electromagnet voltage is increased to 1V; on the second rising edge, to 1.5V, and so on. This sixteen-element list creates a force curve which spans four revolutions of the wheel, assuming the wheel has four pieces of tape attached. The maximum number of revolutions is determined from the length of the bike chain and the circumference of the gear used to drive the wheel.

This sample force curve used during testing is a 31-step stepwise function.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.25]{./images/v-of-h.png}
	\caption{\small Model force curve}
	\label{fig:v-of-h}
\end{figure}

The block diagram of our Labview VI:
\begin{figure}[h]
	\hspace{-0.7in}\includegraphics[scale=0.7]{./images/block-diag-vi.png}
	\label{fig:labview}
\end{figure}

This VI, along with the code used to program the Arduino in the force curve sensor, is available at this gitlab repo: \href{https://gitlab.com/hess2021/eddy-current-brakes-ergometer}{gitlab.com/hess2021/eddy-current-brakes-ergometer}.

\subsection{Electromagnets}

Preliminary estimates of the B-field requried to supply 100lb of braking force, based on the literature and idealized online calculators, were in the range of 0.5-1T. To determine whether we needed to buy pre-built electromagnets, we tested various solenoids around the lab.

No solenoid surpassed a B-field of 40mT at a 1mm distance from the center of an outer face. We built a small one of our own using thicker 19-guage wire, with a length of 135mm and with 560 total turns, but even with a steel core, this magnet did not surpass 70mT, an order of magnitude off from our goal.

The core is vital to B-field strength, as the permeability of the core directly scales the field. An iron core has a permeability of 200 relative to air ($\mu_r = 1$), and can thus easily raise a field's order of magnitude to our requirements. We think that the weakness of our own electromagnet was because the core was made of generic steel, and not iron, and that it did not fill the entire volume of the solenoid. We tried testing one of the student electromagnets, which can reach up to $\approx$ 0.8T, but its physical design was not usable for our project.

Finally, we tested a toroidal electromagnet which reached 0.2T and had iron pole pieces, according to its manual. The physical design of the magnet, made by Atomic Laboratories, was ideal for our setup, except for its large weight. To qualify its braking force, we made a physical pendulum: we secured a 3/8 in. thick sheet of aluminum to a wooden dowel and pivoted the aluminum around the dowel and through the magnet poles (Fig. \ref{fig:prelim-test}). The braking force was remarkable: figure \ref{fig:prelim-test-a} shows maximum amplitude of the pendulum after swinging through the magnet.

This test showed that a proof of concept would certainly be feasible using the Atomic electromagnet. The magnet drew about 1A of current during testing, a safe amount to transmit, even during extensive testing.


\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[scale=0.3]{./images/prelim-test.png}
		\caption{Setup}
		\label{fig:prelim-test}
	\end{subfigure}
	\hspace{3cm}
	\begin{subfigure}[h]{0.4\textwidth}
		\includegraphics[scale=0.27]{./images/prelim-amplitude.png}
		\caption{Maximum amplitude after release}
		\label{fig:prelim-test-a}
	\end{subfigure}
	\caption{Preliminary test}
\end{figure}

\subsection{Brake Design}

Three improvements were made to our preliminary setup to achieve our end product:
\begin{enumerate}[itemsep=0.1mm,parsep=0pt]
	\item increase the thickness of the aluminum to 1/4 in., as suggested by the literature
	\item create an aluminum ``fin'' that wrapped around the entire circumference of the wheel
	\item decrease the distance between the magnet poles to increase the flux density in the aluminum by minimizing fringing effects, as suggested by the literature 
\end{enumerate}

Below is an image of the wheel setup, complete with the object sensor and the necessary reflective surfaces (Fig. \ref{fig:final-setup}). Following that is a photo of the entire ergometer setup, with the chain and force sensor (Fig. \ref{fig:final-setup-large}).

\begin{SCfigure}[0.5][h]
	\centering
	\includegraphics[scale=0.4]{./images/final-setup.png}
	\caption{\small Wheel, magnet (center left), and object sensor (top left wiring)}
	\label{fig:final-setup}
\end{SCfigure}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.37]{./images/final-setup-large.png}
	\caption{\small Force sensor (bottom left) and driving chain}
	\label{fig:final-setup-large}
\end{figure}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data and Interpretation}

\underline{As this} experiment was designed to be used, and thus judged, by a human, so many of the important results are more qualitative than quantitative. As mentioned above, the first points of comparison were existing means of ski imitation, namely a Concept-2 SkiErg. Rollerskiing was used as a proxy for skiing since all the snow had melted by the time data was collected. While there was significant variation in these curves as a result of them being generated by a human user (Luke), however the general shape of the curves was fairly consistent. Due to this, the data was left in sensor voltage vs approximate time, where the former could be calibrated using the curve in Fig. \ref{fig:calibr-curve} and the latter was based of the Adruino’s internal clock which could have been calibrated as well, however since the comparison was of the qualitative shape of the curve, this was not done. Representative samples of each trial are shown in Figs. \ref{fig:sample-curve} and \ref{fig:sample-curve-2}, where there are clear differences in the shape of curves, especially when contextualized by the curves found in literature.

\newpage
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.35]{./images/sample-curve.png}
	\caption{\small Sample of rollerski force curve}
	\label{fig:sample-curve}
\end{figure}
\vspace{2cm}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.30]{./images/sample-curve-2.png}
	\caption{\small Sample of Concept-2 SkiErg force curve}
	\label{fig:sample-curve-2}
\end{figure}
\newpage
\underline{When looking} at the curves being replicated three different characteristics are clear: a slow ‘attack’, a bit of a plateau or even double peak at the top, and a fairly quick finish as the stroke ends. While the specific biomechanics of this were not closely examined, it feels consistent to the experience as a skier. It is immediately obvious that the SkiErg curve does not match these characteristics very well at all. While scales differ due to human elements, sensor issues, and data formatting, it is clear that the shapes of the curves are distinctly different, leaving lots of room to be improved upon.

\underline{Using this} gathered and literature data, as well as some testing of our electromagnetic rig, a model curve was created to be fed into LabView that could control the electromagnet. This is shown in figure \ref{fig:v-of-h}, with the elements of the representative curves discussed earlier, and axis adjusted to match the input voltage of the electromagnet’s power supply and readings of the photodiode. With this input, the resistance of the electromagnetic erg could be tested, using a weight to provide a constant force. Shown in Fig. \ref{fig:data-curve} is the resulting curve measured from a force sensor attached to the weight. While the data is quite messy due to a variety of factors ranging from the added friction and oscillation of the pulley setup to issues with it being at a smaller scale than would be used practically, the added trendline shows that our electromagnet was providing the resistance we expected, even if there were lots of other noisy pieces of the force system. Some of this noise is even shown in the graph, such as the continuing oscillations after the curve.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.35]{./images/data-curve.png}
	\caption{\small Electromagnetically braked erg force curve using the curve from fig. \ref{fig:v-of-h}}
	\label{fig:data-curve}
\end{figure}

\subsection{Error}

For small masses ($<2$kg), we could see our rope oscillating radially. Also, the rope would not move continuously: it would travel slightly ($\approx 1$cm) and then halt briefly, and repeat that, which created discontinuities in its axially-directed motion. We think these radial and axial disturbances are visible in the following data as the oscillation of the measured force around the associated voltage of $1.2$V (Fig. \ref{fig:1kg-graph}).

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.6]{./images/1kg-graph.png}
	\caption{\small Snapshot ($\approx 1$s) of the oscillations in force on a 1kg mass}
	\label{fig:1kg-graph}
\end{figure}

This mass is small enough that the force from the oscillations is large enough to obscure the force from the mass.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}

\underline{This experiment} proved both that existing ergometers are not great representations of forces felt while skiing, and that this can be improved upon using eddy current braking technologies. Measuring forces from roller skiing and a SkiErg revealed notable qualitative differences between the two that could be used to model an improvement. Through the construction of a wheel with eddy current braking responding to the feedback of a photodiode sensor, it was possible to create an improved force curve that was also quite adaptable and modifiable. While the resulting data was quite noisy, it proved the mechanism was working, and much of the noise would not be as noticeable to a human user.

\underline{There is} significant work that could be done to further this experiment. First, and perhaps most obviously in improving the data collection, especially for the final ergometer. With a few extra lab sessions, much could be done to reduce noise and refine the model curve to best represent reality. Building off this, the erg itself could also be refined so that it could be tested by humans, as that is the real test of its effectiveness. While the current setup works well as a proof of concept, it is a poor practical machine, using an energy intensive and heavy electromagnetic, a large bike wheel, and producing what is likely an inefficient resistance for a real workout. Much work could be done to make this more compact and effective in order to create a piece of equipment that could actually be used. Furthermore, steps could be taken to account for things like stroke angle as discussed in the proposal for this project to make it even more representative of really skiing. Finally, it seems likely that other sports such as swimming and rowing would also benefit from using technology to create ergometers that are more specific to their actual actions.
\subsection{Roadmap}

If futures students or entrepeneurs wished to pursue this project further, here are the next steps we would have taken:

\begin{enumerate}[itemsep=0.1mm,parsep=0pt]
	\item Mount the bike wheel on the UnaStrut\texttrademark. This allows you to use the full length of the chain and reduce the error introduced by the rope.
	\item Increase the braking force by adding electromagnets, adding aluminum, or using higher guage wire in the magnets. This is necessary to reduce the percent error of the noise.
	\item Connect the output of the photodiode to a comparator which goes high above 2V. You'll be able to keep the photodiode farther from the wheel and won't have to worry about the two colliding.
	\item If you've increased the braking force, take data with bigger masses.
	\item If you have time, you could figure out how to store a force curve from an existing erg in your Arduino and then have Labview read the curve from the Arduino. Future users could simply bring the Arduino device to a gym, get the curve of a machine they like, and immediately replicate it by plugging the device into this erg.
	\item Test if an electromagnet driven by a alternating current (e.g. 60Hz) would provide greater braking force due to the increased change in magnetic flux through the aluminum (see Karakoc et al., 2015).
\end{enumerate}


\newpage

\section{Works Cited}

Alistar Smith. (n.d.). \emph{Ski Pole with Integrated Sensors for Force and Power Measurement}

(World Intellectual Property Organization Patent No. WO 2017/139897 A1).\\

\noindent Bonde-Petersen, F. (1974a). A new electrically braked bicycle ergometer with low voltage

power supply. \emph{European Journal of Applied Physiology and Occupational Physiology},

33(1), 35–39. \href{https://doi.org/10.1007/BF00423182}{https://doi.org/10.1007/BF00423182}\\

\noindent Chakravorti, N., Conway, P. P., West, A. A., \& Slawson, S. E. (2013). A Novel Instrumented

Cycle Ergometer with Automated In-Situ Capabilities. \emph{2013 IEEE International}

\emph{Conference on Systems, Man, and Cybernetics}, 1599–1604.

\href{https://doi.org/10.1109/SMC.2013.276}{https://doi.org/10.1109/SMC.2013.276}\\

\noindent Gosline, A.H., Campion, G., \& Hayward, V. (2006). \emph{On The Use of Eddy Current Brakes}

\emph{as Tunable, Fast Turn-On Viscous Dampers For Haptic Rendering}.\\

\noindent Johansson, M., Korneliusson, M., \& Lawrence, N. L. (2019). Identifying Cross Country

Skiing Techniques Using Power Meters in Ski Poles. In K. Bach \& M. Ruocco (Eds.), 

\emph{Nordic Artificial Intelligence Research and Development} (Vol. 1056, pp. 52–57).

Springer International Publishing. \href{https://doi.org/10.1007/978-3-030-35664-4_5}{https://doi.org/10.1007/978-3-030-35664-4\_5}\\

\noindent Karakoc, Kerem, Afzal Suleman, and Park, Edward J., \emph{Analytical modeling of eddy current}

\emph{brakes with the application of time varying magnetic fields}, Applied Mathematical

Modelling, Volume 40, Issue 2, 2016, Pages 1168-1179,

\href{https://doi.org/10.1016/j.apm.2015.07.006.}{https://doi.org/10.1016/j.apm.2015.07.006.}\\

\noindent Karpovich, P. V. (1950). A Frictional Bicycle Ergometer. \emph{Research Quarterly. American}

\emph{Association for Health, Physical Education and Recreation}, 21(3), 210–215.

\href{https://doi.org/10.1080/10671188.1950.10624852}{https://doi.org/10.1080/10671188.1950.10624852}\\

\noindent Kickinyov, V. V. (n.d.). \emph{An Automatic Torque Control System for a Bicycle Ergometer}

\emph{Equipped with an Eddy Current Brake}. 1.\\

\noindent Ma, Der-Ming \& Shiau, Jaw-Kuen. (2011). \emph{The design of eddy-current magnet brakes}.

Transactions of the Canadian Society for Mechanical Engineering. 35.

10.1139/tcsme-2011-0002. \\

\noindent Ohtonen, O. (2019). \emph{Biomechanics in cross-country skiing skating technique and}

\emph{measurement techniques of force production}. 124.\\

\noindent Seifert, J. G. (1991). The Comparison of Physiological Responses from Cycling on Friction-

Braked and Electromagnetic Ergometers. \emph{Research Quarterly for Exercise and Sport},

62(1), 115–117. \href{https://doi.org/10.1080/02701367.1991.10607528}{https://doi.org/10.1080/02701367.1991.10607528}\\

\noindent Stöggl, T., \& Holmberg, H.-C. (2011). Force interaction and 3D pole movement in double

poling: Force interaction and 3D in double poling. \emph{Scandinavian Journal of Medicine \&}

\emph{Science in Sports}, 21(6), e393–e404. \href{https://doi.org/10.1111/j.1600-0838.2011.01324.x}{https://doi.org/10.1111/j.1600-0838.2011.01324.x}\\

\noindent Wiederick, Harvey \& Gauthier, Napoleon \& Campbell, Dwight \& Rochon, Paul. (1987).

\emph{Magnetic braking: Simple theory and experiment}. American Journal of Physics - AMER

J PHYS. 55. 500-503. 10.1119/1.15103. \\






%\printbibliography[heading=bibintoc,title={Works Cited}]
\end{document}
